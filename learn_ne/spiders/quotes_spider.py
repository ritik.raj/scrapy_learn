import scrapy
from ..items import LearnNeItem

class QuoteSpider(scrapy.Spider):
    name = 'quotes'
    start_urls = [
        'http://quotes.toscrape.com/'
    ]

    def parse(self,response):

        item = LearnNeItem()
        for quote in response.css('div.quote'):

                text = quote.css('span.text::text')[0].get()
                author = quote.css('small.author::text').get()
                tags = quote.css('div.tags a.tag::text').getall()

                item['text'] = text
                item['author'] = author
                item['tags'] = tags

                yield item
        next_page = response.css('li.next a::attr(href)').get()
        if next_page is not None:
            yield response.follow(next_page,callback=self.parse)

