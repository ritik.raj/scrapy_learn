# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://docs.scrapy.org/en/latest/topics/item-pipeline.html


# useful for handling different item types with a single interface
from itemadapter import ItemAdapter
from pymongo import MongoClient

class LearnNePipeline:

    def __init__(self):
        self.conn = MongoClient('localhost',27017)
        db = self.conn['quotes_db']
        self.tb = db['quotes_tb']

    def process_item(self, item, spider):
        # print('*'*10)
        # print('Pipeline'+item['text'][0])
        self.tb.insert(dict(item))
        return item
